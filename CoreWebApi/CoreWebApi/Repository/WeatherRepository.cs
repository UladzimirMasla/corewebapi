﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CoreWebApi.Repository
{
    public class WeatherRepository
    {
        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };
        public List<WeatherForecast> weatherForecasts;

        public WeatherRepository()
        {
            var rng = new Random();
            weatherForecasts = Enumerable.Range(1, 5).Select(index => new WeatherForecast
            {
                Id = index,
                Date = DateTime.Now.AddDays(index).Date,
                TemperatureC = rng.Next(-20, 55),
                Summary = Summaries[rng.Next(Summaries.Length)]
            })
            .ToList();
        }
    }
}
