﻿using System;
using System.Collections.Generic;
using System.Linq;

using CoreWebApi.Models;

namespace CoreWebApi.Repository
{
    public class GroupRepository
    {
        private static readonly string[] Filler = new[] {
            "POIT", "POIT2", "POIT3", "SOME", "MARKETING", "MANUAL STUFF", "OTHER",
        };

        private static readonly string[] NameFiller = new[] {
            "Uladzimir", "Nikita", "Pavel", "Eugene", "Mike"
        };


        public GroupRepository()
        {
            var rng = new Random();
            Groups = Enumerable.Range(1, 5).Select(index => new Group
            {
                Id = index,
                Description = Filler[rng.Next(Filler.Length)],
                Name = Filler[rng.Next(Filler.Length)],
                Students = GenerateStudents()
            })
            .ToList();
        }

        public List<Group> Groups { get; set; }

        private List<Student> GenerateStudents()
        {
            var rng = new Random();
            var result = Enumerable.Range(1, 5).Select(index => new Student
            {
                Id = index,
                Name = NameFiller[rng.Next(NameFiller.Length)],
                LicenseId = rng.Next(100000).ToString()
            })
            .ToList();

            return result;
        }
    }
}
