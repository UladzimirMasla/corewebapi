﻿namespace CoreWebApi.Models
{
    public class Student
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string LicenseId { get; set; }
    }
}
