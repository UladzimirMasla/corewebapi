﻿using AutoWrapper;

namespace CoreWebApi.Errors
{
    // this class used internally by AutoWrapper
    // find out more here: https://github.com/proudmonkey/AutoWrapper#enable-property-mappings

    public class MapResponseObject
    {
        [AutoWrapperPropertyMap(Prop.ResponseException)]
        public object Error { get; set; }
    }
}
