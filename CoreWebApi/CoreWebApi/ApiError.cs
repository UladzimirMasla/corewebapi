﻿using System.Collections.Generic;

namespace CoreWebApi
{
    public class ApiError
    {
        public int HttpStatus { get; set; }

        public string ErrorCode { get; set; }

        public IEnumerable<string> Errors { get; set; }
    }
}
