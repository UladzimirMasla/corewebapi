﻿using AutoWrapper.Wrappers;
using CoreWebApi.Errors;
using CoreWebApi.Models;
using CoreWebApi.Repository;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net;


namespace CoreWebApi.Controllers
{
    [Route("[controller]")]
    [ApiController]
    [ApiVersionNeutral]
    public class GroupController : ControllerBase
    {
        private GroupRepository _groupRepository;


        public GroupController(GroupRepository groupRepository)
        {
            _groupRepository = groupRepository;
        }


        [HttpGet]
        public ActionResult<IEnumerable<GroupDTO>> Get()
        {
            return _groupRepository.Groups.Select(CreateFrom).ToList();
        }

        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public ActionResult<GroupDTO> GetById(int id)
        {
            var group = _groupRepository.Groups.FirstOrDefault(g => g.Id == id);

            if (group == null)
            {
                return NotFound();
            }

            return CreateFrom(group);
        }

        [HttpPost]
        public ActionResult<GroupDTO> Post([FromBody] GroupDTO group)
        {
            var isGroupWithNameExists = _groupRepository.Groups.FirstOrDefault(g => g.Name == group.Name) != null;
            if (isGroupWithNameExists)
            {
                var error = new Error("Group with provided name already exists", ErrorCodes.AlreadyExist);

                throw new ApiException(error, (int)HttpStatusCode.Conflict);
            }

            var newGroupId = _groupRepository.Groups.Count + 1;

            var groupModel = CreateFrom(group);
            groupModel.Students = new List<Student>();
            groupModel.Id = newGroupId;

            _groupRepository.Groups.Add(groupModel);

            return CreatedAtAction(nameof(GetById), new { id = newGroupId }, CreateFrom(groupModel));
        }

        [HttpPut("{id}")]
        public ActionResult Put(int id, [FromBody] GroupDTO group)
        {
            var oldGroup = _groupRepository.Groups.FirstOrDefault(g => g.Id == id);

            if (oldGroup == null)
            {
                return NotFound();
            }

            // check model state
            // by default if data annotations presented should trigger check internal

            _groupRepository.Groups.Remove(oldGroup);

            oldGroup.Name = group.Name;
            oldGroup.Description = group.Description;

            _groupRepository.Groups.Add(oldGroup);

            return Ok();
        }

        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        public ActionResult Delete(int id)
        {
            var group = _groupRepository.Groups.FirstOrDefault(g => g.Id == id);

            if (group == null)
            {
                return NotFound();
            }

            return NoContent();
        }



        static public GroupDTO CreateFrom(Group group)
        {
            return new GroupDTO
            {
                Id = group.Id,
                Name = group.Name,
                Description = group.Description
            };
        }

        static public Group CreateFrom(GroupDTO groupDTO)
        {
            return new Group
            {
                Name = groupDTO.Name,
                Description = groupDTO.Description
            };
        }



        public class GroupDTO
        {
            public int Id { get; set; }

            [Required]
            public string Name { get; set; }

            [Required]
            public string Description { get; set; }
        }
    }
}
