﻿using CoreWebApi.Models;
using CoreWebApi.Repository;

using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

using System.Collections.Generic;
using System.Linq;

namespace CoreWebApi.Controllers
{
    [Route("/Group/{groupId}/[Controller]")]
    [ApiVersion("1.0")]
    [ApiVersion("2.0")]
    [ApiController]
    public class StudentController : ControllerBase
    {
        private GroupRepository _groupRepository;


        public StudentController(GroupRepository groupRepository)
        {
            _groupRepository = groupRepository;
        }

        /// /// <summary>
        /// Gets all students by group
        /// </summary>
        /// <param name="groupId">The students group id</param>
        /// <returns>An array of students</returns>
        /// <response code="200">Returns student's array</response>
        [HttpGet]
        [MapToApiVersion("1.0")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public ActionResult<IEnumerable<StudentV1>> GetV1(int groupId)
        {
            var students = _groupRepository.Groups
                .Where(g => g.Id == groupId)
                .SelectMany(g => g.Students)
                .Select(CreateFrom)
                .ToList();

            return students;
        }

        /// <summary>
        /// Gets student by Id
        /// </summary>
        /// <param name="id">The student Id</param>
        /// <param name="groupId">The student group Id</param>
        /// <returns>A student</returns>
        /// <response code="200">Returns the student</response>
        /// <response code="404">If non student found</response>
        [HttpGet("{id}")]
        [MapToApiVersion("1.0")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public ActionResult<StudentV1> GetByIdV1(int groupId, int id)
        {
            var student = _groupRepository.Groups.FirstOrDefault(g => g.Id == groupId)?.Students.FirstOrDefault(s => s.Id == id);

            if (student == null)
            {
                return NotFound();
            }

            return CreateFrom(student);
        }

        /// <summary>
        /// Creates a new student.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST /Group/1/Student
        ///     {
        ///        "name": "My Student Name"
        ///     }
        ///
        /// </remarks>
        /// <param name="groupId">The group id</param>
        /// <param name="student">Student itself</param>
        /// <returns>A newly created Student</returns>
        /// <response code="201">Returns the newly created student</response>
        /// <response code="404">If group not found</response>
        [HttpPost]
        [MapToApiVersion("1.0")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public ActionResult<StudentV1> PostV1(int groupId, StudentV1 student)
        {
            var group = _groupRepository.Groups.FirstOrDefault(g => g.Id == groupId);
            if (group == null)
            {
                return NotFound();
            }

            var newStudentId = group.Students.Count + 1;
            student.Id = newStudentId;

            group.Students.Add(CreateFrom(student));

            return CreatedAtAction(nameof(GetById), new { id = newStudentId, groupId = groupId }, student);
        }

        [HttpPut("{id}")]
        [MapToApiVersion("1.0")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public ActionResult PutV1(int groupId, int id, StudentV1 student)
        {
            var group = _groupRepository.Groups.FirstOrDefault(g => g.Id == groupId);
            var studentToUpdate = group?.Students.FirstOrDefault(s => s.Id == id);
            if (group == null || studentToUpdate == null)
            {
                return NotFound();
            }

            group.Students.Remove(studentToUpdate);
            studentToUpdate.Name = student.Name;
            group.Students.Add(studentToUpdate);

            return Ok();
        }


        [HttpGet]
        [MapToApiVersion("2.0")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public ActionResult<IEnumerable<Student>> Get(int groupId)
        {
            var students = _groupRepository.Groups
                .Where(g => g.Id == groupId)
                .SelectMany(g => g.Students)
                .ToList();

            return students;
        }

        [HttpGet("{id}")]
        [MapToApiVersion("2.0")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public ActionResult<Student> GetById(int groupId, int id)
        {
            var student = _groupRepository.Groups.FirstOrDefault(g => g.Id == groupId)?.Students.FirstOrDefault(s => s.Id == id);

            if (student == null)
            {
                return NotFound();
            }

            return student;
        }

        /// <summary>
        /// Creates a new student.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST /Group/1/Student
        ///     {
        ///        "name": "My Student Name",
        ///        "licenseId": "2315454234"
        ///     }
        ///
        /// </remarks>
        /// <param name="groupId">The group id</param>
        /// <param name="student">Student itself</param>
        /// <returns>A newly created Student</returns>
        /// <response code="201">Returns the newly created student</response>
        /// <response code="404">If group not found</response>
        /// <response code="400">If validation exception</response>
        [HttpPost]
        [MapToApiVersion("2.0")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public ActionResult<Student> Post(int groupId, Student student)
        {
            var group = _groupRepository.Groups.FirstOrDefault(g => g.Id == groupId);
            if (group == null)
            {
                return NotFound();
            }

            var isThereTheSameLicenseForStudent = group.Students.Any(st => st.LicenseId == student.LicenseId);

            if(isThereTheSameLicenseForStudent)
            {
                var apiError = new ApiError
                {
                    HttpStatus = 400,
                    ErrorCode = GenericErrorCodes.ValidationError,
                    Errors = new List<string>
                    {
                        "Student with the same license exists",
                    }
                };

                return StatusCode(apiError.HttpStatus, apiError);
            }

            var newStudentId = group.Students.Count + 1;
            student.Id = newStudentId;

            group.Students.Add(student);

            return CreatedAtAction(nameof(GetById), new { id = newStudentId, groupId = groupId }, student);
        }

        [HttpPut("{id}")]
        [MapToApiVersion("2.0")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public ActionResult Put(int groupId, int id, Student student)
        {
            var group = _groupRepository.Groups.FirstOrDefault(g => g.Id == groupId);
            var studentToUpdate = group?.Students.FirstOrDefault(s => s.Id == id);
            if (group == null || studentToUpdate == null)
            {
                return NotFound();
            }

            group.Students.Remove(studentToUpdate);
            studentToUpdate.Name = student.Name;
            studentToUpdate.LicenseId = student.LicenseId;
            group.Students.Add(studentToUpdate);

            return Ok();
        }


        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        public ActionResult Delete(int groupId, int id)
        {
            var group = _groupRepository.Groups.FirstOrDefault(g => g.Id == groupId);
            var student = group?.Students.FirstOrDefault(s => s.Id == id);

            if (group == null || student == null)
            {
                return NotFound();
            }

            group.Students.Remove(student);

            return NoContent();
        }


        private StudentV1 CreateFrom(Student student)
        {
            return new StudentV1
            {
                Id = student.Id,
                Name = student.Name
            };
        }

        private Student CreateFrom(StudentV1 student)
        {
            return new Student
            {
                Id = student.Id,
                Name = student.Name,
                LicenseId = string.Empty
            };
        }



        public class StudentV1
        {
            public int Id { get; set; }

            public string Name { get; set; }
        }
    }
}
