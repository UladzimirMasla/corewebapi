﻿using AutoWrapper.Wrappers;
using CoreWebApi.Errors;
using CoreWebApi.Repository;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;
using System.Net;

namespace CoreWebApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    [ApiVersionNeutral]
    [EnableCors("MyCorsPolicy")]
    public class WeatherForecastController : ControllerBase
    {
        private readonly ILogger<WeatherForecastController> _logger;
        private readonly WeatherRepository _weatherRepository;

        public WeatherForecastController(ILogger<WeatherForecastController> logger, WeatherRepository weatherRepository)
        {
            _logger = logger;

            _weatherRepository = weatherRepository;
        }


        [HttpGet]
        public IEnumerable<WeatherForecast> Get()
        {
            return _weatherRepository.weatherForecasts;
        }

        [HttpGet("{id}")]
        public ActionResult<WeatherForecast> GetById(int id)
        {
            var weather = _weatherRepository.weatherForecasts.FirstOrDefault(w => w.Id == id);

            if(weather == null)
            {
                return NotFound();
            }

            return weather;
        }

        [HttpPost]
        public ActionResult<WeatherForecast> Post(WeatherForecast weather)
        {
            var isWeatherForProvidedDateExists = _weatherRepository.weatherForecasts.FirstOrDefault(w => w.Date == weather.Date) != null;
            if(isWeatherForProvidedDateExists)
            {
                var error = new Error("Weather for this date already exists", ErrorCodes.AlreadyExist);

                throw new ApiException(error, (int)HttpStatusCode.Conflict);
            }

            var newEntityId = _weatherRepository.weatherForecasts.Count + 1;

            _weatherRepository.weatherForecasts.Add(new WeatherForecast
            {
                Date = weather.Date,
                Summary = weather.Summary,
                TemperatureC = weather.TemperatureC,
                Id = newEntityId
            });

            return CreatedAtAction(nameof(GetById), new { id = newEntityId }, weather);
        }
    }
}
