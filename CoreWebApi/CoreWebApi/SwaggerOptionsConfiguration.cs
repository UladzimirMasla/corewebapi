﻿using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;
using System;
using System.IO;
using System.Reflection;

namespace CoreWebApi
{
    public class SwaggerOptionsConfiguration
        : IConfigureNamedOptions<SwaggerGenOptions>
    {
        private readonly IApiVersionDescriptionProvider provider;


        public SwaggerOptionsConfiguration(IApiVersionDescriptionProvider provider)
        {
            this.provider = provider;
        }


        public void Configure(string name, SwaggerGenOptions options)
        {
            Configure(options);

            var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
            var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);

            options.IncludeXmlComments(xmlPath);
        }

        public void Configure(SwaggerGenOptions options)
        {
            foreach (var description in provider.ApiVersionDescriptions)
            {
                options.SwaggerDoc(
                    description.GroupName,
                    CreateVersionInfo(description));
            }
        }


        private static OpenApiInfo CreateVersionInfo(ApiVersionDescription description)
        {
            var apiInfo = new OpenApiInfo()
            {
                Title = "My Student/Group API",
                Version = description.ApiVersion.ToString(),
                Contact = new OpenApiContact
                {
                    Email = "maslauladzimir@gmai.com",
                    Name = "Uladzimir Masla",
                },
                Description = description.IsDeprecated ? "This API version has been deprecated" : string.Empty
            };

            return apiInfo;
        }
    }
}
